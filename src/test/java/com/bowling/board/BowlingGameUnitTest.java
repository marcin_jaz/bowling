package com.bowling.board;

import com.bowling.board.dto.BowlingGame;
import com.bowling.board.dto.Summary;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class BowlingGameUnitTest {

    @Test
    public void shouldReturnZeroAsTotalPointsWhenTheGameIsNotStarted() {
        BowlingGame bowlingGame = new BowlingGame();
        int totalPoints = bowlingGame.getTotalPoints();
        assertThat(totalPoints).isZero();
    }

    @Test
    public void shouldReturnOnePointFromTheFirstRoll() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isOne();
    }

    @Test
    public void shouldReturnTwoPointsFromTheFirstFrameOnTwoAttempts() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(1);
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isEqualTo(2);
    }

    @Test
    public void shouldAddStrikePointsFromTheNextTwoThrows() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        bowlingGame.roll(1);
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isEqualTo(14);
    }

    @Test
    public void shouldAddSparePoints() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(6);
        bowlingGame.roll(4);
        bowlingGame.roll(2);
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isEqualTo(15);
    }

    @Test
    public void shouldAddStrikePointsOverTwoFrames() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        bowlingGame.roll(10);
        bowlingGame.roll(5);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isEqualTo(45);
    }

    @Test
    public void shouldScoreTwentyPointsIfAlwaysOnePinKnocked() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 1);
        rollTenTimes(bowlingGame, 1);
        //when
        int totalPoints = bowlingGame.getTotalPoints();
        //then
        assertThat(totalPoints).isEqualTo(20);
    }

    private void rollTenTimes(BowlingGame bowlingGame, int pinsNumber) {
        for (int i = 0; i < 10; i++) {
            bowlingGame.roll(pinsNumber);
        }
    }

    @Test
    public void shouldNotOfferThirdRollInLastFrameWhenNoStrikeNorSpareThere() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 1);
        rollTenTimes(bowlingGame, 1);
        //when then
        assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> bowlingGame.roll(1));
    }

    @Test
    public void shouldOfferThirdRollInLastFrameWhenSpareThere() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 5);
        rollTenTimes(bowlingGame, 5);
        //when
        bowlingGame.roll(1);
        //then no exception thrown
    }

    @Test
    public void shouldScoreMaximumThreeHundredPointsWithThePerfectGameOnTwelveStrikes() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 10);
        bowlingGame.roll(10);
        bowlingGame.roll(10);
        //when
        Summary summary = bowlingGame.getSummary();
        //then
        assertThat(summary.getTotalPoints()).isEqualTo(300);
        assertThat(summary.getStrikes()).isEqualTo(12);
        assertThat(summary.getSpares()).isEqualTo(0);
        assertThat(summary.getMisses()).isEqualTo(0);
        assertThat(summary.getTotalPins()).isEqualTo(120);
    }

    @Test
    public void shouldSumAllMissesAndPins() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 0);
        rollTenTimes(bowlingGame, 3);
        //when
        Summary summary = bowlingGame.getSummary();
        //then
        assertThat(summary.getStrikes()).isEqualTo(0);
        assertThat(summary.getSpares()).isEqualTo(0);
        assertThat(summary.getMisses()).isEqualTo(10);
        assertThat(summary.getTotalPins()).isEqualTo(30);
    }

    @Test
    public void shouldSumAllSparesAndLastStrike() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        rollTenTimes(bowlingGame, 5);
        rollTenTimes(bowlingGame, 5);
        bowlingGame.roll(10);
        //when
        Summary summary = bowlingGame.getSummary();
        //then
        assertThat(summary.getStrikes()).isEqualTo(1);
        assertThat(summary.getSpares()).isEqualTo(10);
        assertThat(summary.getMisses()).isEqualTo(0);
        assertThat(summary.getTotalPins()).isEqualTo(110);
    }

    @Test
    public void shouldNotAllowToRollMorePinsThanTenInOneRoll() {
        BowlingGame bowlingGame = new BowlingGame();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> bowlingGame.roll(11));
    }

    @Test
    public void shouldNotAllowToRollMorePinsThanTenInTwoRolls() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(9);
        //when then
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> bowlingGame.roll(2));
    }

    @Test
    public void shouldNotAllowToRollLessThanZeroPins() {
        BowlingGame bowlingGame = new BowlingGame();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> bowlingGame.roll(-1));
    }
}

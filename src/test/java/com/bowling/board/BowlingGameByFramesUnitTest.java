package com.bowling.board;

import com.bowling.board.dto.BowlingGame;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BowlingGameByFramesUnitTest {

    @Test
    public void shouldReturnNumberOfPointsForTheFirstFrame() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(5);
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(0);
        //then
        assertThat(totalPoints).isEqualTo(6);
    }

    @Test
    public void shouldReturnNumberOfPointsForTheFirstFrameWithStrike() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        bowlingGame.roll(1);
        bowlingGame.roll(2);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(0);
        //then
        assertThat(totalPoints).isEqualTo(13);
    }

    @Test
    public void shouldReturnNumberOfPointsForTheFirstFrameWithStrikeWhenSecondFrameNotPlayed() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(0);
        //then
        assertThat(totalPoints).isEqualTo(10);
    }

    @Test
    public void shouldReturnNumberOfPointsForTheSecondFrame() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        bowlingGame.roll(1);
        bowlingGame.roll(2);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(1);
        //then
        assertThat(totalPoints).isEqualTo(3);
    }

    @Test
    public void shouldReturnZeroNumberOfPointsForTheSecondFrameWhenItsNotPlayedYet() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(10);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(1);
        //then
        assertThat(totalPoints).isZero();
    }

    @Test
    public void shouldReturnNumberOfPointsForTheFirstFrameWithSparePoints() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(6);
        bowlingGame.roll(4);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(0);
        //then
        assertThat(totalPoints).isEqualTo(10);
    }

    @Test
    public void shouldReturnNumberOfPointsForTheSecondFrameWhenFirstFrameWasSpare() {
        //given
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.roll(6);
        bowlingGame.roll(4);
        bowlingGame.roll(1);
        //when
        int totalPoints = bowlingGame.getPointsByFrame(0);
        //then
        assertThat(totalPoints).isEqualTo(11);
    }

}

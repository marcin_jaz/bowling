package com.bowling.board;

import com.bowling.board.dto.Frame;
import com.bowling.board.dto.IFrame;
import com.bowling.board.dto.LastFrame;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FrameNextRollMaximumPinsUnitTest {

    @Test
    public void shouldAllow10IfStrikeWasFirst() {
        //given
        IFrame frame = new Frame(null);
        frame.roll(10);
        //when
        int nextRollMaxPins = frame.getNextRollMaxPins();
        //then
        assertThat(nextRollMaxPins).isEqualTo(10);
    }

    @Test
    public void shouldAllow3If7WasFirstRolled() {
        //given
        IFrame frame = new Frame(null);
        frame.roll(7);
        //when
        int nextRollMaxPins = frame.getNextRollMaxPins();
        //then
        assertThat(nextRollMaxPins).isEqualTo(3);
    }

    @Test
    public void shouldAllow3If7WasFirstRolledInLastFrame() {
        //given
        IFrame frame = new LastFrame();
        frame.roll(7);
        //when
        int nextRollMaxPins = frame.getNextRollMaxPins();
        //then
        assertThat(nextRollMaxPins).isEqualTo(3);
    }

    @Test
    public void shouldAllow3InLastRollIf7WasRolledAsSecondInLastFrame() {
        //given
        IFrame frame = new LastFrame();
        frame.roll(10);
        frame.roll(7);
        //when
        int nextRollMaxPins = frame.getNextRollMaxPins();
        //then
        assertThat(nextRollMaxPins).isEqualTo(3);
    }

    @Test
    public void shouldAllow10IfSpareWasRolledInLastFrame() {
        //given
        IFrame frame = new LastFrame();
        frame.roll(4);
        frame.roll(6);
        //when
        int nextRollMaxPins = frame.getNextRollMaxPins();
        //then
        assertThat(nextRollMaxPins).isEqualTo(10);
    }

}

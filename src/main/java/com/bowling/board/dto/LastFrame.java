package com.bowling.board.dto;

public class LastFrame extends AbstractFrame {

    private Roll roll3;

    @Override
    public boolean isStrike() {
        return isFirstRollStrike() || isSecondRollStrike();
    }

    private boolean isFirstRollStrike() {
        return roll1 != null && roll1.isStrike();
    }

    private boolean isSecondRollStrike() {
        return roll2 != null && roll2.isStrike();
    }

    @Override
    public boolean isSpare() {
        return !isFirstRollStrike() && getPinsThrownInTwoRolls() == ALL_PINS_NUMBER;
    }

    @Override
    public Roll[] getRolls() {
        return new Roll[]{roll1, roll2, roll3};
    }

    @Override
    public boolean isFinished() {
        if (isThirdStrikeAllowed()) {
            return roll3 != null;
        }
        return roll2 != null;
    }

    @Override
    public void roll(int pinsNumber) {
        if (roll1 == null) {
            roll1 = new Roll(pinsNumber);
        } else if (roll2 == null) {
            if (!isStrike()) {
                throwIfTooManyPinsRolled(pinsNumber);
            }
            roll2 = new Roll(pinsNumber);
        } else if (roll3 == null && isThirdStrikeAllowed()) {
            roll3 = new Roll(pinsNumber);
        } else {
            throw new UnsupportedOperationException("It's not supported to set pins number for the frame that is already finished.");
        }
    }

    @Override
    public IFrame getNextFrame() {
        return null;
    }

    private boolean isThirdStrikeAllowed() {
        return isStrike() || isSpare();
    }

    @Override
    public int getPinsThrownInTwoRolls() {
        return getFirstRollPins() + getRoll2PinsThrown();
    }

    @Override
    public int getFirstRollPins() {
        return roll1 != null ? roll1.getPins() : 0;
    }

    public int getRoll2PinsThrown() {
        return roll2 != null ? roll2.getPins() : 0;
    }

    public int getThirdRollPins() {
        return roll3 != null ? roll3.getPins() : 0;
    }

    @Override
    public int getPoints() {
        return getPinsThrownInTwoRolls() + getThirdRollPins();
    }

    @Override
    public String toString() {
        return "LastFrame{" +
                "roll1=" + roll1 +
                ", roll2=" + roll2 +
                ", roll3=" + roll3 +
                ", points=" + getPoints() +
                '}';
    }

    protected int getTotalPins() {
        return getPoints();
    }

    @Override
    public int getNextRollMaxPins() {
        if (roll1 == null || (isFirstRollStrike() && roll2 == null) || isSpare() || isSecondRollStrike()) {
            return ALL_PINS_NUMBER;
        } else if (roll2 == null) {
            return ALL_PINS_NUMBER - roll1.getPins();
        }
        return ALL_PINS_NUMBER - roll2.getPins();
    }
}

package com.bowling.board.dto;

import java.util.Objects;
import java.util.stream.Stream;

abstract class AbstractFrame implements IFrame {

    protected Roll roll1;
    protected Roll roll2;

    protected void throwIfTooManyPinsRolled(int pinsNumber) {
        if (roll1.getPins() + pinsNumber > ALL_PINS_NUMBER) {
            throw new IllegalArgumentException("It's impossible to throw " + pinsNumber + " pins if in the first row you have thrown " + roll1.getPins());
        }
    }

    @Override
    public int getPinsThrownInTwoRolls() {
        return getFirstRollPins() + getSecondRollPins();
    }

    @Override
    public int getFirstRollPins() {
        return roll1 != null ? roll1.getPins() : 0;
    }

    protected int getSecondRollPins() {
        return roll2 != null ? roll2.getPins() : 0;
    }


    public Summary getSummary() {
        return Summary.Builder.aSummary()
                .withMisses(getMisses())
                .withStrikes(getStrikesCount())
                .withSpares(isSpare() ? 1 : 0)
                .withTotalPins(getTotalPins())
                .withTotalPoints(getPoints())
                .build();
    }

    private int getMisses() {
        return Stream.of(getRolls())
                .filter(Objects::nonNull)
                .mapToInt(roll -> roll.isMiss() ? 1 : 0)
                .sum();
    }

    private int getStrikesCount() {
        return Stream.of(getRolls())
                .filter(Objects::nonNull)
                .mapToInt(roll -> roll.isStrike() ? 1 : 0)
                .sum();
    }

    protected abstract int getTotalPins();
}

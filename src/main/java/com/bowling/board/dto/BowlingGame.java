package com.bowling.board.dto;

import java.util.stream.Stream;

import static com.bowling.board.dto.IFrame.ALL_PINS_NUMBER;

public class BowlingGame {

    private static final int MAXIMUM_FRAMES_NUMBER = 10;

    private final IFrame[] frames = new IFrame[MAXIMUM_FRAMES_NUMBER];
    private int currentFrameIndex = 0;

    public BowlingGame() {
        frames[MAXIMUM_FRAMES_NUMBER - 1] = new LastFrame();
        for (int i = MAXIMUM_FRAMES_NUMBER - 2; i >= 0; i--) {
            frames[i] = new Frame(frames[i + 1]);
        }
    }

    private IFrame getFrameForNextRoll() {
        IFrame currentFrame = frames[currentFrameIndex];
        if (currentFrame.isFinished()) {
            return frames[++currentFrameIndex];
        }
        return currentFrame;
    }

    public int getNextRollMaxPins() {
        if (isFinished()) {
            return 0;
        }
        return getFrameForNextRoll().getNextRollMaxPins();
    }

    public void roll(int pinsNumber) {
        if (isFinished()) {
            throw new UnsupportedOperationException("The game is already finished.");
        } else if (pinsNumber < 0 || pinsNumber > ALL_PINS_NUMBER) {
            throw new IllegalArgumentException("There are only " + ALL_PINS_NUMBER + " pins so you can't throw " + pinsNumber);
        }
        getFrameForNextRoll().roll(pinsNumber);
    }

    public int getTotalPoints() {
        return Stream.of(frames).mapToInt(IFrame::getPoints).sum();
    }

    public Summary getSummary() {
        return Stream.of(frames).map(IFrame::getSummary).reduce(Summary::add).get();
    }

    public boolean isFinished() {
        return frames[MAXIMUM_FRAMES_NUMBER - 1].isFinished();
    }

    public IFrame[] getFrames() {
        return frames;
    }

    public IFrame getFrameByIndex(int index) {
        if (frames.length <= index) {
            throw new IllegalArgumentException("Number of frames: " + frames.length + ", requested index out of bounds: " + index);
        }
        return frames[index];
    }

    public int getPointsByFrame(int index) {
        return getFrameByIndex(index).getPoints();
    }
}

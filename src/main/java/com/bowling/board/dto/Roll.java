package com.bowling.board.dto;

import static com.bowling.board.dto.IFrame.ALL_PINS_NUMBER;

class Roll {

    private final int pins;

    public Roll(int pins) {
        this.pins = pins;
    }

    public int getPins() {
        return pins;
    }

    public boolean isStrike() {
        return pins == ALL_PINS_NUMBER;
    }

    @Override
    public String toString() {
        return "Roll{" +
                "pins=" + pins +
                '}';
    }

    public boolean isMiss() {
        return pins == 0;
    }
}

package com.bowling.board.dto;

public class Frame extends AbstractFrame {

    private final IFrame nextFrame;

    public Frame(IFrame nextFrame) {
        this.nextFrame = nextFrame;
    }

    @Override
    public boolean isStrike() {
        return roll1 != null && roll1.isStrike();
    }

    @Override
    public boolean isSpare() {
        return !isStrike() && getPinsThrownInTwoRolls() == ALL_PINS_NUMBER;
    }

    public boolean isFinished() {
        return isStrike() || roll2 != null;
    }

    @Override
    public void roll(int pinsNumber) {
        if (roll1 == null) {
            roll1 = new Roll(pinsNumber);
        } else if (roll2 == null || !isStrike()) {
            throwIfTooManyPinsRolled(pinsNumber);
            roll2 = new Roll(pinsNumber);
        } else {
            throw new UnsupportedOperationException("It's not supported to set pins number for the frame that is already finished.");
        }
    }

    @Override
    public IFrame getNextFrame() {
        return nextFrame;
    }

    @Override
    public Roll[] getRolls() {
        return new Roll[]{roll1, roll2};
    }

    @Override
    public int getNextRollMaxPins() {
        if (roll1 == null || isStrike()) {
            return ALL_PINS_NUMBER;
        }
        return ALL_PINS_NUMBER - roll1.getPins();
    }

    @Override
    public int getPoints() {
        int result = getPinsThrownInTwoRolls();
        if (isStrike()) {
            result += getStrikeBonus();
        } else if (isSpare()) {
            result += getSpareBonus();
        }
        return result;
    }

    private int getStrikeBonus() {
        IFrame nextFrame = getNextFrame();
        int result = nextFrame.getPinsThrownInTwoRolls();
        if (nextFrame.isStrike()) {
            IFrame framePlusTwo = nextFrame.getNextFrame();
            if (framePlusTwo != null) {//here we can go beyond the last frame so the check is needed
                result += framePlusTwo.getFirstRollPins();
            }
        }
        return result;
    }

    private int getSpareBonus() {
        return getNextFrame().getFirstRollPins();
    }

    @Override
    public String toString() {
        return "Frame{" +
                "roll1=" + roll1 +
                ", roll2=" + roll2 +
                ", points=" + getPoints() +
                '}';
    }

    protected int getTotalPins() {
        return getPinsThrownInTwoRolls();
    }
}

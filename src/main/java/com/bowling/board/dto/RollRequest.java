package com.bowling.board.dto;

/**
 * Command object for the UI binding, can't be immutable
 */
public class RollRequest {
    private int pinsKnocked;

    public int getPinsKnocked() {
        return pinsKnocked;
    }

    public void setPinsKnocked(int pinsKnocked) {
        this.pinsKnocked = pinsKnocked;
    }
}

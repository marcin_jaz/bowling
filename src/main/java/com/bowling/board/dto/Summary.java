package com.bowling.board.dto;

public class Summary {
    private int misses;
    private int spares;
    private int strikes;
    private int totalPoints;
    private int totalPins;

    public Summary add(Summary other) {
        return Summary.Builder.aSummary()
                .withMisses(misses + other.getMisses())
                .withSpares(spares + other.getSpares())
                .withStrikes(strikes + other.getStrikes())
                .withTotalPins(totalPins + other.getTotalPins())
                .withTotalPoints(totalPoints + other.getTotalPoints())
                .build();
    }

    public int getMisses() {
        return misses;
    }

    public int getSpares() {
        return spares;
    }

    public int getStrikes() {
        return strikes;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public int getTotalPins() {
        return totalPins;
    }

    public static final class Builder {
        private int misses;
        private int spares;
        private int strikes;
        private int totalPoints;
        private int totalPins;

        private Builder() {
        }

        public static Builder aSummary() {
            return new Builder();
        }

        public Builder withMisses(int misses) {
            this.misses = misses;
            return this;
        }

        public Builder withSpares(int spares) {
            this.spares = spares;
            return this;
        }

        public Builder withStrikes(int strikes) {
            this.strikes = strikes;
            return this;
        }

        public Builder withTotalPoints(int totalPoints) {
            this.totalPoints = totalPoints;
            return this;
        }

        public Builder withTotalPins(int totalPins) {
            this.totalPins = totalPins;
            return this;
        }

        public Summary build() {
            Summary summary = new Summary();
            summary.strikes = this.strikes;
            summary.misses = this.misses;
            summary.totalPoints = this.totalPoints;
            summary.totalPins = this.totalPins;
            summary.spares = this.spares;
            return summary;
        }
    }

    @Override
    public String toString() {
        return "Summary{" +
                "misses=" + misses +
                ", spares=" + spares +
                ", strikes=" + strikes +
                ", totalPins=" + totalPins +
                ", totalPoints=" + totalPoints +
                '}';
    }
}

package com.bowling.board.dto;

public interface IFrame {

    int ALL_PINS_NUMBER = 10;

    boolean isStrike();

    boolean isSpare();

    int getPoints();

    boolean isFinished();

    /**
     * Update the frame with the number of pins thrown.
     *
     * @param pinsNumber number of pins thrown
     */
    void roll(int pinsNumber);

    IFrame getNextFrame();

    int getFirstRollPins();

    int getPinsThrownInTwoRolls();

    Roll[] getRolls();

    Summary getSummary();

    /**
     * @return maximum number of pins that can be rolled in the next roll. It takes into account
     * if there was strike/spare before
     */
    int getNextRollMaxPins();
}

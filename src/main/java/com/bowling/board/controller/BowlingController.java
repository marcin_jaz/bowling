package com.bowling.board.controller;

import com.bowling.board.dto.BowlingGame;
import com.bowling.board.dto.RollRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BowlingController {

    private static final String REDIRECT_TO_HOME = "redirect:/";

    @Value("${spring.application.name}")
    private String title;

    private BowlingGame bowlingGame = new BowlingGame();

    @GetMapping("/")
    public String showBoard(Model model) {
        model.addAttribute("game", bowlingGame);
        model.addAttribute("rollRequest", new RollRequest());
        return "home";
    }

    @PostMapping("/roll")
    public String roll(@ModelAttribute RollRequest rollRequest) {
        bowlingGame.roll(rollRequest.getPinsKnocked());
        return REDIRECT_TO_HOME;
    }

    @GetMapping("/restart")
    public String restart() {
        bowlingGame = new BowlingGame();
        return REDIRECT_TO_HOME;
    }
}
